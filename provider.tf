terraform {
  # cloud {
  #   organization = "chrisline_engineering"
  #   workspaces {
  #     name = "production"
  #   }
  }
  # required_version = ">= 1.3.8"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }

  }
}




 provider "aws" {
   region = "us-east-1"
  #profile = "default"
}

# terraform {
#   backend "s3" {
#     # Replace this with your bucket name!
#     bucket         = "s3.backend"
#     key            = "newstate.tfstate"
#     region         = "us-east-1"

#     # Replace this with your DynamoDB table name!
#     dynamodb_table = "backend-table"
#   }
#  }


